#!/bin/sh

curl -o /tmp/anchore_ci_tools.py https://raw.githubusercontent.com/anchore/ci-tools/v0.2.4/scripts/anchore_ci_tools.py
chmod +x /tmp/anchore_ci_tools.py
ln -s /tmp/anchore_ci_tools.py /usr/local/bin/anchore_ci_tools
anchore_ci_tools --setup
anchore-cli --u admin --p foobar registry add "$CI_REGISTRY" gitlab-ci-token "$CI_JOB_TOKEN" --skip-validate

for image in `docker service ls --format '{{ .Image }}' | sort | uniq`
    do echo "running anchore scan for $image"
    image_name_version=`echo $image | awk -F "/" '{print$2}' | sed -s 's/:/-/g'`
    mkdir -p $image_name_version
    cd $image_name_version
    echo "generating reports in `pwd`"
    anchore_ci_tools --analyze --report --image "$image" --timeout "$ANCHORE_TIMEOUT"
    cd ../
done