FROM ubuntu:16.04
LABEL maintainer "Sheshagiri Rao Mallipedhi <msheshagirirao@gmail.com>"

RUN apt-get update
RUN apt-get install -y make wget
RUN apt-get update && apt-get install -y software-properties-common && apt-get install -y gcc && apt-get install -y gdb && apt-get install -y vim
RUN cd /tmp && \
wget https://dl.google.com/go/go1.10.linux-amd64.tar.gz && \
tar -xvf go1.10.linux-amd64.tar.gz && \
mv go /usr/local  
